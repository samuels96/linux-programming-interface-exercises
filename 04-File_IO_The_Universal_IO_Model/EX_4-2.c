#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include "tlpi_hdr.h"

#define BUF_SIZE 1024

int main(int argc, char *argv[] ){
    int inFd, outFd;
    int holeSize = 0;
    int numRead;
    char *buf;
    buf = malloc(BUF_SIZE);

    if (argc < 3 || strcmp(argv[1], "--help") == 0)
        usageErr("%s {input file} {output file}...\n",
                 argv[0]);
    inFd = open(argv[1], O_RDONLY);
    if(inFd == -1)
        errExit("open");

    outFd = open(argv[2], O_RDWR | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                S_IROTH | S_IWOTH);         
    if(outFd == -1)
        errExit("open");

    while((numRead = read(inFd, buf, BUF_SIZE)) > 0){

        for( int i = 0; i < numRead; i ++){
            if( buf[i] == '\0' ){
                holeSize++;
            }
            else if( holeSize > 0 ){
                lseek(outFd, holeSize, SEEK_CUR);
                write(outFd, &buf[i], 1);
                holeSize = 0;
            }
            else{
                write(outFd, &buf[i], 1);
            }
        }
    }

    if(close(inFd) == -1){
        errExit("close");
    }

    if(close(outFd) == -1){
        errExit("close");
    }

} 
