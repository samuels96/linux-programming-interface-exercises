
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include "tlpi_hdr.h"

#define BUF_SIZE 1024

int main(int argc, char *argv[] ){
    char *buf; 
    buf = malloc(BUF_SIZE);
    int fd;
    int numRead = 0;
    ssize_t numWritten = 0;
    char c;

    fd = open(argv[1], O_RDWR | O_CREAT,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
                S_IROTH | S_IWOTH);                 
    if(fd == -1)
        errExit("open");

    lseek(fd, 0, SEEK_END);

    while( ( c =getchar() ) != '\n'){
        buf[numRead++] = c;
    }

    buf[numRead] = '\0';

    numWritten = write(fd, buf, numRead);
    if( numWritten == -1 ){
        errExit("write");
    }

    printf("Bytes written: %ld, String written: %s\n", (long)numWritten, buf);

    if(close(fd) == -1)
        errExit("close");
} 
